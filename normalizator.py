import math
import numpy as np
import cv2 as cv


def normalize_video(npz):
    video = npz['colorImages']
    normalized_video = []
    box_size = get_box_size(npz['boundingBox'])
    average_image = None
    for i in range(video.shape[-1]):
        frame = cv.cvtColor(np.ascontiguousarray(video[..., i], dtype=np.uint8), cv.COLOR_RGB2BGR)
        box = npz['boundingBox'][..., i]
        landmarks = npz['landmarks2D'][..., i]

        frame = align_eyes(frame, landmarks, box, box_size)
        frame = crop_frame(frame, box, box_size)
        frame = cv.resize(frame, (100, 100))

        average_image = update_average_image(average_image, frame, i)

        normalized_video.append(frame)

    return normalized_video, average_image


def update_average_image(average_image, frame, alpha_i):
    if average_image is None:
        return frame

    alpha = 1.0/(alpha_i + 1)
    beta = 1.0 - alpha

    # print(frame.shape)
    # print(average_image.shape)
    # cv.imshow('average', average_image)
    # cv.imshow('frame', frame)
    # cv.waitKey(0)

    average_image = cv.addWeighted(frame, alpha, average_image, beta, 0.0)

    return average_image


def align_eyes(frame, landmarks, box, box_size):
    """source: https://sefiks.com/2020/02/23/face-alignment-for-face-recognition-in-python-within-opencv/"""
    left_eye, right_eye = get_eyes_centres(landmarks)
    point_3, rotation_point, matrix_eye, direction = get_align_info(left_eye, right_eye)

    angle = get_angle(left_eye, right_eye, point_3, direction)
    rotation_matrix = cv.getRotationMatrix2D(rotation_point, angle, 1.0)
    frame = cv.warpAffine(frame, rotation_matrix, frame.shape[1::-1], flags=cv.INTER_LINEAR)

    matrix = get_eyes_matrix(box, box_size, matrix_eye)
    frame = cv.warpAffine(frame, matrix, (frame.shape[1], frame.shape[0]))

    return frame


def get_eyes_matrix(box, box_size, eye):
    y = int(box[0][1]) + int(box_size[1] / 3)
    x = int(box[0][0]) + int(box_size[0] / 3)
    x_diff = x - eye['x']
    y_diff = y - eye['y']
    return np.float32([[1, 0, x_diff], [0, 1, y_diff]])


def get_angle(left_eye, right_eye, point_3, direction):
    a = get_distance(left_eye, point_3)
    b = get_distance(right_eye, left_eye)
    c = get_distance(right_eye, point_3)
    cos_a = (b * b + c * c - a * a) / (2 * b * c)
    angle = (np.arccos(cos_a) * 180) / math.pi
    if direction == -1:
        angle = 90 - angle
    return angle * direction


def get_align_info(left_eye, right_eye):
    if left_eye['y'] > right_eye['y']:
        point_3 = {
            'x': right_eye['x'],
            'y': left_eye['y']
        }
        matrix_eye = left_eye
        rotation_point = (left_eye['x'], left_eye['y'])
        direction = -1
    else:
        point_3 = {
            'x': left_eye['x'],
            'y': right_eye['y']
        }
        matrix_eye = point_3
        rotation_point = (right_eye['x'], right_eye['y'])
        direction = 1
    return point_3, rotation_point, matrix_eye, direction


def get_distance(point1, point2):
    return math.sqrt(
        ((point2['x'] - point1['x']) * (point2['x'] - point1['x']))
        +
        ((point2['y'] - point1['y']) * (point2['y'] - point1['y']))
    )


def get_eyes_centres(landmarks):
    left_x = int((int(landmarks[36][0]) + int(landmarks[39][0])) / 2)
    left_y = int((int(landmarks[36][1]) + int(landmarks[39][1])) / 2)
    left_eye = {
        'x': left_x,
        'y': left_y
    }
    right_x = int((int(landmarks[42][0]) + int(landmarks[45][0])) / 2)
    right_y = int((int(landmarks[42][1]) + int(landmarks[45][1])) / 2)
    right_eye = {
        'x': right_x,
        'y': right_y
    }
    return left_eye, right_eye


def get_box_size(boxes):
    max_width = 0
    max_height = 0
    for i in range(boxes.shape[-1]):
        box = boxes[..., i]
        width = int(box[3][0]) - int(box[0][0])
        height = int(box[3][1]) - int(box[0][1])
        if width > max_width:
            max_width = width
            max_height = width
        if height > max_height:
            max_width = height
            max_height = height
    return max_width, max_height


def crop_frame(frame, box, box_size):
    y1 = int(box[0][1])
    y2 = y1 + box_size[1]  # int(box[3][1])
    x1 = int(box[0][0])
    x2 = x1 + box_size[0]  # int(box[3][0])
    return frame[y1:y2, x1:x2]