import pathlib
import csv
import os
import cv2 as cv
import numpy as np
from sklearn.decomposition import PCA


def save_features(pairs, pair_type, mode='all'):
    if mode in ['all', 'pca']:
        save_pca_features(pairs, pair_type)
    if mode in ['all', 'hog']:
        save_hog_features(pairs, pair_type)


def save_hog_features(pairs, pair_type):
    print('\n\n---------------------\nSTART SAVING HOG FEATURES of', len(pairs) * 2, 'VIDEOS: ', pair_type)
    path = str(pathlib.Path(__file__).parent.resolve()) + '/data/pairs/' + pair_type + '/'
    counter = 1
    for pair in pairs:
        save_hog_video_features(pair[0], path + 'videoA/')
        print(counter, '. video features saved')
        counter += 1
        save_hog_video_features(pair[1], path + 'videoB/')
        print(counter, '. video features saved')
        counter += 1
    print('CSV FILES CREATED')


def save_hog_video_features(video_name, path):
    csv_path = path + 'csv_hog/' + video_name + '.csv'
    with open(csv_path, 'w', newline='\n', encoding='UTF8') as file:
        writer = csv.writer(file, delimiter=',')
        writer.writerow(['frame', 'descriptors'])
        image_path = path + 'average/' + video_name + '.jpg'
        write_hog_row_csv(writer, 'average.jpg', image_path)
        for image_name in os.listdir(path + video_name):
            image_path = path + video_name + '/' + image_name
            write_hog_row_csv(writer, image_name, image_path)


def write_hog_row_csv(writer, image_name, image_path):
    """source: https://medium.com/swlh/histogram-of-oriented-gradients-hog-for-multiclass-image-classification-and-image-recommendation-cf0ea2caaae8"""
    image = cv.imread(image_path)
    hog = get_hog()
    features = hog.compute(image)
    writer.writerow([image_name, features.tolist()])


def get_hog():
    winSize = (20,20)
    blockSize = (10,10)
    blockStride = (5,5)
    cellSize = (10,10)
    nbins = 9
    derivAperture = 1
    winSigma = -1.
    histogramNormType = 0
    L2HysThreshold = 0.2
    gammaCorrection = 1
    nlevels = 64
    signedGradient = True

    return cv.HOGDescriptor(winSize,blockSize,blockStride,cellSize,nbins,derivAperture,winSigma,histogramNormType,L2HysThreshold,gammaCorrection,nlevels, signedGradient)


def save_pca_features(pairs, pair_type):
    print('\n\n---------------------\nSTART SAVING PCA FEATURES of', len(pairs) * 2, 'VIDEOS: ', pair_type)
    path = str(pathlib.Path(__file__).parent.resolve()) + '/data/pairs/' + pair_type + '/'
    pca_eigenfaces, pca_mean = get_pca_features(pairs, path)
    print('EIGENFACES CALCULATED')
    counter = 1
    for pair in pairs:
        save_pca_video_features(pair[0], path + 'videoA/', pca_eigenfaces, pca_mean)
        print(counter, '. video features saved')
        counter += 1
        save_pca_video_features(pair[1], path + 'videoB/', pca_eigenfaces, pca_mean)
        print(counter, '. video features saved')
        counter += 1
    print('CSV FILES CREATED')


def save_pca_video_features(video_name, path, pca_eigenfaces, pca_mean):
    csv_path = path + 'csv_pca/' + video_name + '.csv'
    with open(csv_path, 'w', newline='\n', encoding='UTF8') as file:
        writer = csv.writer(file, delimiter=',')
        writer.writerow(['frame', 'descriptors'])
        image_path = path + 'average/' + video_name + '.jpg'
        write_pca_row_csv(writer, 'average.jpg', image_path, pca_eigenfaces, pca_mean)
        for image_name in os.listdir(path + video_name):
            image_path = path + video_name + '/' + image_name
            write_pca_row_csv(writer, image_name, image_path, pca_eigenfaces, pca_mean)


def write_pca_row_csv(writer, image_name, image_path, pca_eigenfaces, pca_mean):
    image = cv.imread(image_path)
    weights = pca_eigenfaces @ (image.flatten() - pca_mean).T
    writer.writerow([image_name, weights])


def get_pca_features(pairs, path):
    all_images = get_all_images(pairs, path)
    n_components = 128
    pca = PCA(n_components=n_components).fit(all_images)
    pca_eigenfaces = pca.components_[:n_components]
    return pca_eigenfaces, pca.mean_


def get_all_images(pairs, path):
    all_images = []
    for pair in pairs:
        add_video_images(all_images, path + 'videoA/' + pair[0])
        add_video_images(all_images, path + 'videoB/' + pair[1])
        add_image(all_images, path + 'videoA/average/' + pair[0] + '.jpg')
        add_image(all_images, path + 'videoB/average/' + pair[1] + '.jpg')
    return all_images


def add_video_images(all_images, path):
    for image_name in os.listdir(path):
        image_path = path + '/' + image_name
        add_image(all_images, image_path)


def add_image(all_images, image_path):
    image = cv.imread(image_path)
    all_images.append(np.array(image).flatten())
