from loader import load_csv_pairs, save_pairs
from extractor import save_features
from comparator import compare_pairs, create_roc

false_pairs = load_csv_pairs('FalsePairs', 10)
true_pairs = load_csv_pairs('TruePairs', 10)

save_pairs(false_pairs, 'false', 2)
save_pairs(true_pairs, 'true', 2)

save_features(false_pairs, 'false')
save_features(true_pairs, 'true')

compare_pairs(false_pairs, 'false')
compare_pairs(true_pairs, 'true')

create_roc()


