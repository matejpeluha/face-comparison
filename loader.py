import csv
import pathlib
import numpy as np
from normalizator import normalize_video
import os
import cv2 as cv


def load_csv_pairs(file_name, length):
    pairs = []
    path = str(pathlib.Path(__file__).parent.resolve()) + '/data/' + file_name + '.csv'
    with open(path, newline='') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        next(reader)
        for row in reader:
            if 'Marieta_Chrousala_1.npz' not in row:
                first = row[0].replace('.npz', '')
                second = row[1].replace('.npz', '')
                pairs.append([first, second])
            if len(pairs) == length:
                return pairs
    return pairs


def save_pairs(pairs, pair_type, nth_frame=1):
    print('\n\n-----------------------------\nSTART SAVING', len(pairs) * 2, 'VIDEOS: ', pair_type)
    parent_path = str(pathlib.Path(__file__).parent.resolve()) + '/data'
    create_folders(parent_path)
    parent_path += '/pairs'

    counter = 1
    for pair in pairs:
        video_type = '/videoA/'
        for name in pair:
            video, average_imageA = load_npz_video(name + '.npz')
            path = parent_path + '/' + pair_type + video_type
            save_video(video, path + name, nth_frame)
            save_image(average_imageA, path + 'average/' + name + '.jpg')
            video_type = '/videoB/'
            print(counter, '. video saved')
            counter += 1
    print('VIDEOS SAVED')


def save_video(frames, path, nth_frame):
    if not os.path.exists(path):
        os.makedirs(path)
    image_counter = 0
    for frame_index in range(0, len(frames), nth_frame):
        image = frames[frame_index]
        image_path = path + '/' + str(image_counter) + '.jpg'
        save_image(image, image_path)
        image_counter += 1


def save_image(image, path):
    cv.imwrite(path, image)


def create_folders(parent_path):
    if not os.path.exists(parent_path + '/pairs'):
        os.makedirs(parent_path + '/pairs')

    parent_path += '/pairs'
    if not os.path.exists(parent_path + '/false'):
        os.makedirs(parent_path + '/false')
    if not os.path.exists(parent_path + '/true'):
        os.makedirs(parent_path + '/true')

    if not os.path.exists(parent_path + '/false/comparisons_pca'):
        os.makedirs(parent_path + '/false/comparisons_pca')
    if not os.path.exists(parent_path + '/true/comparisons_pca'):
        os.makedirs(parent_path + '/true/comparisons_pca')
    if not os.path.exists(parent_path + '/false/comparisons_hog'):
        os.makedirs(parent_path + '/false/comparisons_hog')
    if not os.path.exists(parent_path + '/true/comparisons_hog'):
        os.makedirs(parent_path + '/true/comparisons_hog')

    if not os.path.exists(parent_path + '/false/videoA'):
        os.makedirs(parent_path + '/false/videoA')
    if not os.path.exists(parent_path + '/true/videoA'):
        os.makedirs(parent_path + '/true/videoA')
    if not os.path.exists(parent_path + '/false/videoB'):
        os.makedirs(parent_path + '/false/videoB')
    if not os.path.exists(parent_path + '/true/videoB'):
        os.makedirs(parent_path + '/true/videoB')

    if not os.path.exists(parent_path + '/false/videoA/average'):
        os.makedirs(parent_path + '/false/videoA/average')
    if not os.path.exists(parent_path + '/true/videoA/average'):
        os.makedirs(parent_path + '/true/videoA/average')
    if not os.path.exists(parent_path + '/false/videoB/average'):
        os.makedirs(parent_path + '/false/videoB/average')
    if not os.path.exists(parent_path + '/true/videoB/average'):
        os.makedirs(parent_path + '/true/videoB/average')

    if not os.path.exists(parent_path + '/false/videoA/csv_pca'):
        os.makedirs(parent_path + '/false/videoA/csv_pca')
    if not os.path.exists(parent_path + '/true/videoA/csv_pca'):
        os.makedirs(parent_path + '/true/videoA/csv_pca')
    if not os.path.exists(parent_path + '/false/videoB/csv_pca'):
        os.makedirs(parent_path + '/false/videoB/csv_pca')
    if not os.path.exists(parent_path + '/true/videoB/csv_pca'):
        os.makedirs(parent_path + '/true/videoB/csv_pca')

    if not os.path.exists(parent_path + '/false/videoA/csv_hog'):
        os.makedirs(parent_path + '/false/videoA/csv_hog')
    if not os.path.exists(parent_path + '/true/videoA/csv_hog'):
        os.makedirs(parent_path + '/true/videoA/csv_hog')
    if not os.path.exists(parent_path + '/false/videoB/csv_hog'):
        os.makedirs(parent_path + '/false/videoB/csv_hog')
    if not os.path.exists(parent_path + '/true/videoB/csv_hog'):
        os.makedirs(parent_path + '/true/videoB/csv_hog')


def load_npz_video(video_name):
    path = str(pathlib.Path(__file__).parent.resolve()) + '/data/videos/' + video_name
    npz = np.load(path)
    video = normalize_video(npz)
    return video
