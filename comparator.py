import pathlib
from random import randint
import numpy
import numpy as np
import csv
import cv2 as cv
import matplotlib.pyplot as plt

"""
COMPARE FEATURES
"""


def compare_pairs(pairs, pair_type):
    print('\n\n---------------------------------------------\nSTART COMPARING: ', pair_type)
    path = str(pathlib.Path(__file__).parent.resolve()) + '/data/pairs/' + pair_type + '/'
    csv_results = [[], [], [], [], [], []]
    csv.field_size_limit(1000000)
    for pair in pairs:
        print('\n############### compare ', pair[0], ' vs ', pair[1], '###############')
        compare(pair, path, csv_results)
    save_csv_results(csv_results, path)


def save_csv_results(csv_results, path):
    path1 = path + '/comparisons_pca/average_results.csv'
    path2 = path + '/comparisons_hog/average_results.csv'
    path3 = path + '/comparisons_pca/average_image_results.csv'
    path4 = path + '/comparisons_hog/average_image_results.csv'
    path5 = path + '/comparisons_pca/random_image_results.csv'
    path6 = path + '/comparisons_hog/random_image_results.csv'
    paths = [path1, path2, path3, path4, path5, path6]
    for index in range(6):
        path = paths[index]
        results = csv_results[index]
        with open(path, 'w', newline='\n', encoding='UTF8') as file:
            writer = csv.writer(file, delimiter=',')
            writer.writerow(['image_1', 'image_2', 'distance'])
            for row in results:
                writer.writerow(row)


def compare(pair, path, csv_results):
    print('\n..............PCA:')
    compare_average(pair, path, 'csv_pca', csv_results[2])
    compare_random_frames(pair, path, 'csv_pca', csv_results[4])
    compare_all_frames(pair, path, 'csv_pca', csv_results[0])
    print('\n..............HOG:')
    compare_average(pair, path, 'csv_hog', csv_results[3])
    compare_random_frames(pair, path, 'csv_hog', csv_results[5])
    compare_all_frames(pair, path, 'csv_hog', csv_results[1])


def compare_all_frames(pair, path, csv_type, all_results):
    path1 = path + 'videoA/' + csv_type + '/' + pair[0] + '.csv'
    path2 = path + 'videoB/' + csv_type + '/' + pair[1] + '.csv'
    average_distance = 0
    comparison_counter = 0
    best_distance = -1
    best_frame1 = ""
    best_frame2 = ""

    with open(path1, 'r', newline='') as csv_file1:
        reader1 = csv.reader(csv_file1, delimiter=',')
        next(reader1)
        for row1 in reader1:
            if row1[0] == 'average.jpg':
                continue
            vector1 = string_vector_to_ndarray(row1[1])
            with open(path2, 'r', newline='') as csv_file2:
                reader2 = csv.reader(csv_file2, delimiter=',')
                next(reader2)
                for row2 in reader2:
                    if row2[0] == 'average.jpg':
                        continue
                    vector2 = string_vector_to_ndarray(row2[1])
                    distance = numpy.linalg.norm(vector1 - vector2, axis=0)
                    average_distance += distance
                    comparison_counter += 1
                    if distance < best_distance or best_distance == -1:
                        best_distance = distance
                        best_frame1 = row1[0]
                        best_frame2 = row2[0]
    average_distance /= comparison_counter

    save_image_comparisons(pair[0], best_frame1, pair[1], best_frame2, path, csv_type.replace('csv_', ''),
                           best_distance)
    print('most similar frames are [', best_frame1, ' vs ', best_frame2, '] with distance: ', best_distance)
    all_results.append([pair[0], pair[1], average_distance])
    print('average distance of all images: ', average_distance)


def save_image_comparisons(name1, frame1, name2, frame2, path, method_type, distance):
    image_path = path + '/comparisons_' + method_type + '/' + name1 + '_vs_' + name2 + '-' + str(distance) + '.jpg'
    path1 = path + 'videoA/' + name1 + '/' + frame1
    path2 = path + 'videoB/' + name2 + '/' + frame2
    image1 = cv.imread(path1)
    image2 = cv.imread(path2)
    new_image = cv.hconcat([image1, image2])
    cv.imwrite(image_path, new_image)


def compare_average(pair, path, csv_type, average_image_results):
    path1 = path + 'videoA/' + csv_type + '/' + pair[0] + '.csv'
    vector1, frame1 = get_vector(path1, 'average.jpg')

    path2 = path + 'videoB/' + csv_type + '/' + pair[1] + '.csv'
    vector2, frame2 = get_vector(path2, 'average.jpg')

    distance = numpy.linalg.norm(vector1 - vector2, axis=0)
    average_image_results.append([pair[0], pair[1], distance])
    print('distance of average face [', frame1, ' vs ', frame2, ']: ', distance)


def compare_random_frames(pair, path, csv_type, random_image_results):
    frame1 = str(randint(0, 100)) + '.jpg'
    path1 = path + 'videoA/' + csv_type + '/' + pair[0] + '.csv'
    vector1, frame1 = get_vector(path1, frame1)

    frame2 = str(randint(0, 100)) + '.jpg'
    path2 = path + 'videoB/' + csv_type + '/' + pair[1] + '.csv'
    vector2, frame2 = get_vector(path2, frame2)

    distance = numpy.linalg.norm(vector1 - vector2, axis=0)
    random_image_results.append([pair[0], pair[1], distance])
    print('distance of random frames [', frame1, ' vs ', frame2, ']: ', distance)


def get_vector(path, row_name):
    last_row = []
    with open(path, 'r', newline='') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for row in reader:
            if row[0] == row_name:
                return string_vector_to_ndarray(row[1]), row[0]
            else:
                last_row = row
    return string_vector_to_ndarray(last_row[1]), row[0]


def string_vector_to_ndarray(string_vector):
    vector = string_vector.replace('[', '').replace('\n', '')
    vector = np.fromstring(vector, dtype=float, sep=' ')
    return vector


"""
ROC
"""


def create_roc():
    print('\n\n------------------------------------CREATING ROC------------------------------------')
    path = str(pathlib.Path(__file__).parent.resolve()) + '/data/pairs/'
    data_pca = get_roc_data('pca', path)
    print('# PCA DATA EXTRACTED: \n', data_pca)
    data_hog = get_roc_data('hog', path)
    print('# HOG DATA EXTRACTED: \n', data_hog)

    init_plot('PCA')
    plot_data(data_pca[2], 'distance from random image')
    plot_data(data_pca[1], 'distance from average image')
    plot_data(data_pca[0], 'average distance from all images')
    plt.show()

    init_plot('HOG')
    plot_data(data_hog[2], 'distance from random image')
    plot_data(data_hog[1], 'distance from average image')
    plot_data(data_hog[0], 'average distance from all images')
    plt.show()


def init_plot(title):
    plt.title(title)
    plt.plot([0, 1], [0, 1])
    plt.xlabel('FALSE POSITIVE RATE')
    plt.ylabel('TRUE POSITIVE RATE')


def plot_data(data, label):
    x = []
    y = []
    for row in data:
        y.append(row[0])
        x.append(row[1])

    plt.plot(x, y, label=label)
    plt.legend(loc="lower right")


def get_roc_data(method, path):
    average_data = get_roc_data_by_type(method, path, 'average')
    average_image_data = get_roc_data_by_type(method, path, 'average_image')
    random_image_data = get_roc_data_by_type(method, path, 'random_image')
    return average_data, average_image_data, random_image_data


def get_roc_data_by_type(method, path, results_type):
    tresholds = get_tresholds(method)
    data = []

    for treshold in tresholds:
        tpr = get_positive_rate(path, method, 'true', results_type, treshold)
        fpr = get_positive_rate(path, method, 'false', results_type, treshold)
        data.append([tpr, fpr, treshold])

    return data


def get_tresholds(method):
    if method == 'pca':
        return range(5500, 20000, 200)
    elif method == 'hog':
        return [number / 100 for number in range(0, 100, 1)]
    else:
        return []


def get_positive_rate(path, method, bool_type, results_type, treshold):
    total_count = 0
    positive_count = 0
    csv_path = path + bool_type + '/comparisons_' + method + '/' + results_type + '_results.csv'
    with open(csv_path, 'r', newline='') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        next(reader)
        for row in reader:
            total_count += 1
            if float(row[2]) < treshold:
                positive_count += 1
    return positive_count / total_count
